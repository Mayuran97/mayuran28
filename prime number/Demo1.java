import java.util.Scanner;
public class Demo1 {
	public static void main(String args[]) {
		String prime;
		do {
			System.out.println(" Enter your number : ");
			Scanner scan = new Scanner(System.in);

			int num = scan.nextInt();
			if (num < 0) { 			// this condition is used for number must be positive

				System.out.println(" ### Number must be positive ### ");
			} else {
				Boolean primeNo = Boolean.TRUE;
				for (int i = 2; i < num; i++) {

					if (num % i == 0) { 			// prime number condition

						primeNo = Boolean.FALSE;
						break;
					}
				}
				if (primeNo == Boolean.TRUE) {
					System.out.println(" ***** Prime Number ***** ");
				} else {
					System.out.println(" !!! This is not a prime number !!! ");
				}
			}
			System.out.println();
			System.out.println("Do you want to continue? if yes type 'y' or type 'n' ");
			prime = scan.next();
			if (prime.equals("n")) {
			System.out.println(" *** Bye *** ");
			}
		}	 while (prime.equals("y"));
	}
}
